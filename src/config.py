"""Module containing utilities

"""
import cv2

IMG_PATH = "/home/lfridez/DEV/hes/python-image-processing/img/"


def getImagePath(img):
    """Get a filepath in image src folder

    Author : Fridez Lucas
    """
    return f"{IMG_PATH}{img}"


def saveImage(img, filename):
    """Save an image in dist folder

    Author : Fridez Lucas
    """
    cv2.imwrite(f"{IMG_PATH}dist/{filename}", img)
