import cv2
import numpy as np
import config as hes

#region Utils

NBR_STEP = 5

COLORS = {
    0: "red",
    1: "green",
    2: "blue"
}

def goNextStep(nbr, nbrTot):
    print(f"[Step {nbr}/{nbrTot}] Press any key...\n")
    cv2.waitKey(0)
    cv2.destroyAllWindows()

#endregion

#region Ex1
def loadAndDisplayColorImage(imagePath):
    img = cv2.imread(imagePath)
    cv2.imshow("Lena", img)
    goNextStep(1, NBR_STEP)

def importColorImageAsGray(imagePath):
    img = cv2.imread(imagePath, cv2.IMREAD_GRAYSCALE)
    cv2.imshow("Lena grayscaled with imread", img)
    goNextStep(2, NBR_STEP)

def importColorImageAndConvert(imagePath):
    img = cv2.imread(imagePath)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    cv2.imshow("Lena grayscaled with conversion", gray)
    hes.saveImage(gray, "lenyGray.jpg")
    goNextStep(3, NBR_STEP)

def saveGrayImage(image, grayImagePath):
    cv2.imwrite(grayImagePath, image)
    pass

#endregion

#region Ex2

def getColorName(color):
    return COLORS[color]

def splitImageOpenCV(image):
    img = cv2.imread(hes.getImagePath(image))

    cv2.imshow("Colored image", img)

    color = 0
    for c in cv2.split(img):
        print(c)
        cv2.imshow(f"Split with OpenCV method : {getColorName(color)}", c) 
        color += 1
    
    goNextStep(4, NBR_STEP)

def displaySplitImageNumpy(image):
    img = cv2.imread(hes.getImagePath(image))

    cv2.imshow("Colored image", img)

    for c in range(len(COLORS)):
        print(c)
        cv2.imshow(f"Split with numpy array : {getColorName(c)}", img[:, :, c]) 
    
    goNextStep(4, NBR_STEP)

#endregion

#region main
def main():
    # Get Lena image
    img = hes.getImagePath("lena.png")

    # Do ex 1
    loadAndDisplayColorImage(img)
    importColorImageAsGray(img)
    importColorImageAndConvert(img)

    # Do ex 2
    splitImageOpenCV("colors.png")
    displaySplitImageNumpy("colors.png")

    # Close all
    cv2.waitKey(0)

#endregion

if __name__ == '__main__':
    main()