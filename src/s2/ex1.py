import cv2
import numpy as np
import config as hes
from scipy import stats

# region Utils

NBR_STEP = 5

IMGS = [
    {"img": "lena", "path": hes.getImagePath("lena.png")},
    {"img": "baboon", "path": hes.getImagePath("baboon.png")},
    {"img": "colors", "path": hes.getImagePath("colors.png")}
]

# region Logic


def printImageDetails(img):
    # Values
    definition = getDefinition(img)
    imgType = getType(img)

    # Print values
    printDefinition(definition)
    printType(imgType)
    printSize(definition, imgType)
    printChannels(definition)

    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    printMin(gray)
    printMax(gray)
    printMean(gray)
    printStandardDeviation(gray)
    printMode(gray)


def getDefinition(img):
    # return {"x": len(img), "y": len(img[0])}
    return img.shape


def printDefinition(definition):
    print(f"\tdefinition = {definition}")


def getType(img):
    return img.dtype


def printType(imgType):
    print(f"\ttype = {imgType}")


def printSize(definition, type):
    print(
        f"\tsize = {definition[0]}x{definition[1]}x{definition[2]} = {definition[0] * definition[1] * definition[2]}")


def printChannels(definition):
    print(f"\tchannels = {definition[2]}")


def printMin(grayImage):
    print(f"\tmin = {np.amin(grayImage)}")


def printMax(grayImage):
    print(f"\tmax = {np.amax(grayImage)}")


def printMean(grayImage):
    print(f"\tmean = {np.mean(grayImage)}")


def printStandardDeviation(grayImage):
    print(f"\tstd. deviation = {np.std(grayImage)}")


def printMode(grayImage):
    #print(f"\tmode = {np.argmax(np.bincount(grayImage.flatten()))}")
    print(f"\tmode = {stats.mode(grayImage, axis=None).mode}")

# endregion

# region main


def main():

    c = 1
    for i in IMGS:
        print(f"[{c}/{len(IMGS)}] : {i['img']}")
        img = cv2.imread(i['path'])

        printImageDetails(img)
        c += 1

# endregion


if __name__ == "__main__":
    main()
