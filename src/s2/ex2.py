import cv2
import numpy as np
import config as hes
from matplotlib import pyplot as plt

lenaImg = cv2.imread(hes.getImagePath("lena.png"))
lenaImgGray = cv2.cvtColor(lenaImg, cv2.COLOR_BGR2GRAY)

# region Grayscale


def printGrayscaleHistogram(img):

    plt.subplot(221), plt.imshow(img, "gray")

    # cv2
    plt.subplot(222)
    histr = cv2.calcHist([img], [0], None, [256], [0, 256])
    plt.plot(histr)
    plt.xlim([0, 256])
    plt.ylim(ymin=0)

    # np
    plt.subplot(223)
    hist, bins = np.histogram(img.ravel(), 256, [0, 256])
    plt.plot(hist)
    plt.ylim(ymin=0)

    # plt
    plt.subplot(224)
    plt.hist(img.ravel(), 256, [0, 256])

    plt.xlim([0, 256])
    plt.show()

# endregion


# region Colors

def printColorsHistogram(img):
    colors = ('b', 'g', 'r')
    for i, col in enumerate(colors):
        histr = cv2.calcHist([img], [i], None, [256], [0, 256])
        plt.plot(histr, color=col)
        plt.xlim([0, 256])
        plt.ylim(ymin=0)
    plt.show()

# endregion


# region main

def main():
    printGrayscaleHistogram(lenaImgGray)
    printColorsHistogram(lenaImg)


if __name__ == "__main__":
    main()


# endregion
