import numpy as np
from numpy.core.shape_base import block
from scipy import stats
from matplotlib import pyplot as plt
import config as hes
import cv2

# region Utils

IMGS = {
    "lena": hes.getImagePath("LenaX.png")
}

# endregion

# region main


def main():
    lena = cv2.imread(IMGS["lena"], 0)

    mask = 255
    for i in range(1, 9):
        mask -= 2 ** i
        maskedImage = cv2.bitwise_and(lena, mask)
        hist, bins = np.histogram(maskedImage.ravel(), 256, [0, 256])
        plt.subplot(211)
        plt.plot(hist)
        plt.ylim(ymin=0)

        # histogram
        plt.subplot(212)
        plt.imshow(maskedImage, cmap="gray")
        plt.show()
        
    cv2.waitKey(0)

# endregion


if __name__ == "__main__":
    main()