import numpy as np
from scipy import stats
from matplotlib import pyplot as plt
import config as hes
import cv2

# region Utils

IMGS = {
    "ref": hes.getImagePath("BackgroudSubtraction/image_ref.bmp")
}

for i in range(1, 25):
    IMGS[i] = hes.getImagePath(f"BackgroudSubtraction/image_{(str)(i).zfill(2)}.bmp")
# endregion

# region main


def main():
    ref = cv2.imread(IMGS["ref"])
    for i in range(1, 25):
        imgTest = cv2.imread(IMGS[i])
        result = ref - imgTest
        plt.imshow(result, cmap="gray")
        plt.show()

# endregion


if __name__ == "__main__":
    main()