import numpy as np
from scipy import stats
from matplotlib import pyplot as plt
import config as hes
import cv2

# region Utils

IMGS = {
    "A": hes.getImagePath("100dollarsA.tif"),
    "C": hes.getImagePath("100dollarsC.tif")
}

# endregion

# region main


def main():
    lena = cv2.imread(IMGS["C"], 0)
    mask = 255
    for i in range(1, 9):
        mask -= 2 ** i
        maskedImage = cv2.bitwise_and(lena, mask)
        plt.imshow(maskedImage, cmap="gray")
        plt.show()

# endregion


if __name__ == "__main__":
    main()