import numpy as np
from scipy import stats
from matplotlib import pyplot as plt
import config as hes
import cv2

# region Utils

IMGS = {
    "lena": hes.getImagePath("LenaX.png")
}

# endregion

# region main


def main():
    lena = cv2.imread(IMGS["lena"], 0)
    for i in range(1, 9):
        mask = 2 ** i
        maskedImage = cv2.bitwise_and(lena, mask)
        plt.imshow(maskedImage, cmap="gray")
        plt.show()

# endregion


if __name__ == "__main__":
    main()