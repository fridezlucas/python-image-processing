import cv2
import numpy as np
from scipy import stats
from matplotlib import pyplot as plt
import config as hes

# region Utils

IMGS = {
    "lena": hes.getImagePath("lena10.pgm"),
    "marseille": hes.getImagePath("marseilleBW.tif"),
    "boat": hes.getImagePath("boat_lowContrast_NB_512x512.tif")

}

# endregion

# region main


def main():

    for img in IMGS:
        img = cv2.imread(IMGS[img])
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        plt.subplot(221), plt.imshow(img, "gray")

        # normalized
        plt.subplot(222)
        imgNormalized = cv2.normalize(img, None, 0, 255, cv2.NORM_MINMAX)
        plt.imshow(imgNormalized, "gray")

        # qualize
        plt.subplot(223)
        imgEqualized = cv2.equalizeHist(img)
        plt.imshow(imgEqualized, "gray")

        # CLAHE
        clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))
        imagCLAHE = clahe.apply(img)
        plt.subplot(224)
        plt.imshow(imagCLAHE, "gray")

        plt.show()

# endregion


if __name__ == "__main__":
    main()
