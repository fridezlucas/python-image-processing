import cv2
import numpy as np
from numpy.core.records import array
from numpy.lib.function_base import copy
from scipy import stats
from matplotlib import pyplot as plt
import config as hes
from copy import deepcopy

# region Utils

IMGS = {
    "lena": hes.getImagePath("lena10.pgm"),
    "boat": hes.getImagePath("boat_lowContrast_NB_512x512.tif")
}

# endregion


# region Logic

def getInvertLookupTable():
    return np.arange(255, -1, -1)


def negativeLena():
    img = cv2.imread(IMGS["lena"])
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    plt.subplot(121), plt.imshow(img, "gray")

    # inverted
    plt.subplot(122)
    table = getInvertLookupTable()
    imgNormalized = table[img].astype(np.uint8)
    plt.imshow(imgNormalized, "gray")
    plt.show()


def getMaxX(data):
    for p in range(255, 0, -1):
        if data[p] != 0:
            return p


def getMinX(data):
    for p in range(255):
        if data[p] != 0:
            return p


def getContrastTable(min, max):
    table = np.zeros(256, dtype=np.uint8)
    for i in range(255):
        if(i < min):
            table[i] = 0
        elif(i > max):
            table[i] = 255
        else:
            table[i] = round(255 * (i - min)/(max - min))
    return array(table)


def contrastBoat():
    imgSrc = cv2.imread(IMGS["boat"])
    img = cv2.cvtColor(imgSrc, cv2.COLOR_BGR2GRAY)
    cv2.imshow("gray", img)

    # histogram
    hist, bins = np.histogram(img.ravel(), 256, [0, 256])

    minX = getMinX(hist)
    maxX = getMaxX(hist)

    # inverted
    plt.subplot(223)
    table = getContrastTable(minX, maxX)
    contrastedImage = table[img].astype(np.uint8)
    cv2.imshow("better contrast", contrastedImage)

    # plot first hist
    plt.subplot(211)
    plt.plot(hist)
    plt.ylim(ymin=0)

    # histogram
    plt.subplot(212)
    hist, bins = np.histogram(contrastedImage.ravel(), 256, [0, 256])
    plt.plot(hist)
    plt.ylim(ymin=0)
    plt.show()

# endregion

# region main


def main():
    negativeLena()
    contrastBoat()
    cv2.waitKey(0)


# endregion


if __name__ == "__main__":
    main()
