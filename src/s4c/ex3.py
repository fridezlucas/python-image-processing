import numpy as np
from scipy import stats
from matplotlib import pyplot as plt
import config as hes
import cv2

# region Utils

IMGS = {
    "lena": hes.getImagePath("LenaX.png")
}

FILTERS = [
    {
        "title": "h1",
        "kernel": np.array(np.ones((3, 3),np.float32))
    },
    {
        "title": "h2",
        "kernel": np.array([[-1, 0, 1], [-1, 0, 1], [-1, 0, 1]])
    },
    {
        "title": "h3",
        "kernel": np.array([[-1, -1, -1], [0, 0, 0], [1, 1, 1]])
    },
    {
        "title": "h4",
        "kernel": np.array([[-1, -1, -1], [-1, 8, -1], [-1, -1, -1]])
    },
    {
        "title": "h5",
        "kernel": np.array([[-1, -1, -1], [-1, 12, -1], [-1, -1, -1]])
    },
]

# endregion

# region main


def main():
    lena = cv2.imread(IMGS["lena"], cv2.IMREAD_GRAYSCALE)

    for f in FILTERS:
        s = sum(sum(f["kernel"]))
        k = f["kernel"]
        if s != 0:
            k = k / s
        result = cv2.filter2D(lena, -1, k)
        hes.saveImage(result, f"{f['title']}.png")

# endregion


if __name__ == "__main__":
    main()
