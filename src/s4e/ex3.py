import numpy as np
from scipy import stats
from matplotlib import pyplot as plt
import config as hes
import cv2
from copy import deepcopy

# region Utils

IMGS = {
    "1balle": hes.getImagePath("UneBalleBleue.jpg"),
    "3balles": hes.getImagePath("TroisBallesRougeBleueBlanche.jpg")
}

# endregion


def show_image(img, title):
    cv2.imshow(title, img)
    cv2.waitKey(0)


def close_image(img):
    element_structured = np.ones((6, 6), np.uint8)
    cv2.dilate(img, element_structured, iterations=2, dst=img)
    cv2.erode(img, element_structured, iterations=2, dst=img)
    return img


LOWER_BLUE = np.array([120, 140, 140])
UPPER_BLUE = np.array([130, 255, 255])


def show_blue_ball(img):
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

    # Blue parts
    blue_part = cv2.inRange(hsv, LOWER_BLUE, UPPER_BLUE)
    show_image(blue_part, "Blue parts")

    # Dilate then Erode => closed
    closed = deepcopy(blue_part)
    closed = close_image(closed)
    show_image(closed, "Closed image")

    # Mark houghts
    result = cv2.bitwise_xor(closed, blue_part)
    show_image(result, "Result")


def main():
    show_blue_ball(cv2.imread(IMGS["1balle"]))
    show_blue_ball(cv2.imread(IMGS["3balles"]))


if __name__ == "__main__":
    main()
